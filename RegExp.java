
/*****
 * Created by Julia Cassella and Maeve McClatchey

 * 
 * COMP 370
 * 
 * PA3 : Regular Expressions
 *  Searching text for string that are in the language of a regular expression
 * 
 * Due May 12, 2017
 * 
 *****/

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;

public class RegExp {
	private static String alph;
	private static String output_fileName;
	private static Stack<String> operator;
	private static Stack<Tree> operand;
	private static String[] expression;
	private static String ops;
	private static Tree syntax;
	private static ArrayList<String> states;
	private static ArrayList<String> accept;
	private static String start;
	private static ArrayList<String> transition;

	private static int Q; // the number of states
	private static String[] alphabet; // stores each element of alphabet
	private static int alph_length;
	private static String[][] trans; // double array of state to alphabet
	// transitions for NFA
	private static String[] accept_states;
	private static String[][] DFA_trans; // double array of DFA transitions
	private static ArrayList<String> dfa_states; // list of all dfa states in
	// set notation
	private static ArrayList<String> dfa_accept; // list of accepted DFA states
	private static String dump = "%"; // This will be used to signify a dump
	// state

	private static int B; // length of alphabet
	private static int num_transitions;
	private static String[][] transitions;
	private static String[] acceptingStates;
	private static int start_state;
	private static int currentState;
	private static boolean accepting;
	private static BufferedReader br;

	public static void main(String[] args) {
		

		// initialize states
		states = new ArrayList<String>();

		transition = new ArrayList<String>();

		ops = "()|#*";

		// the first command line will read in the file from the user
		String inputfilename = args[0];
		

		// the second command line will read in the print file from the user
		output_fileName = args[1];

		try {
			FileReader fr = new FileReader(inputfilename);
			BufferedReader br = new BufferedReader(fr);

			// read in the first line which will contain the alphabet and find
			// the length of the alphabet \
			alph = br.readLine() + "e";
			operand = new Stack<Tree>();
			operator = new Stack<String>();

			// read in second line containing regular expression and set into
			// array
			String input = br.readLine().replaceAll(" ", "");

			if (input.equals("e")) {
				syntax = new Tree("e");
			}

			else if (input.equals("N")) {
				syntax = new Tree("N");
			}

			else {

				// check if there are and eqaul number of open and closed paren
				int open = 0;
				int close = 0;

				for (int i = 0; i < input.length(); i++) {
					if (input.charAt(i) == ')')
						close++;
					else if (input.charAt(i) == '(')
						open++;
				}

				//if there is an unequal number then we need to print invalid expression to the file 
				if (open != close) {

					ArrayList<String> not_needed = new ArrayList<String>();

					writeToFile(1, not_needed);
					System.exit(-1);
				}

				//otherwise we need to split the expression and run it through the set up tree method
				expression = input.split("");
				syntax = setTree(expression);

			}

			//with our completed syntax tree we now run our make_NFA method
			make_NFA(syntax);
			

			// Now we have to handle if there are any transitions out of the
			// last state that it takes us to a dump state
			int dump_state = states.size() + 1;
			states.add(dump_state + "");

			// loop through the alphabet (accept e)
			for (int i = 0; i < alph.length() - 1; i++) {
				// now loop through all the ending states
				for (int k = 0; k < syntax.end.size(); k++) {
					String tran = syntax.end.get(k) + " '" + alph.charAt(i) + "' " + dump_state;
					transition.add(tran);

					syntax.transition.add(tran);
				}
			}

			// now set the transitions for the dump state
			for (int k = 0; k < alph.length(); k++) {
				String tran = dump_state + " '" + alph.charAt(k) + "' " + dump_state;
				transition.add(tran);
				syntax.transition.add(tran);
			}

			//with the NFA made we can now run it through code used in our PA2
			run_NFA();

			//read in lines from the file 
			String line = br.readLine();
			String result = "";
			ArrayList<String> print = new ArrayList<>();

			//print the results to a file 
			while (line != null) {
				result = (check(line));
				print.add(result);
				line = br.readLine();
			}
			writeToFile(0, print);
		} catch (IOException e) {
			
			System.exit(-1);
		}
	}

	/**
	 * Method will go through and print the tree
	 * 
	 * Note that this is only needed in debugging the code
	 * 
	 * @param Tree
	 *            that we are going to be printing
	 */
	public static void printTree(Tree t) {
		

		if (t.right != null) {
			printTree(t.right);
		}

		if (t.left != null) {
			printTree(t.left);
		}
	}

	/**
	 * Method that will set the subtree from the array that stores the
	 * expression
	 * 
	 * @param exp
	 *            the array that has the expression i.e 1|2*
	 * @return our Expression tree
	 */
	public static Tree setTree(String[] exp) {
		// loop through exp
		// if operand, add to stack with operand as root
		// check validity
		for (int i = 0; i < exp.length; i++) {
			invalidExp(exp, i);

			//check to see if we are handeling an operator
			if (ops.contains(exp[i])) {
				if (!(operator.isEmpty())) {

					// encounter (
					if (exp[i].equals("(")) {

						//If the expresion had an ) before it such as )( then we need to concat those terms
						if (i != 0 && exp[i - 1].equals(")")) {

							operator.push("#");
						}

						// we want to see if the parenthesis was proceeded by a
						// letter in the alph such as a( 

						else if (i != 0 && alph.contains(exp[i - 1])) {
							operator.push("#");

						}

						// if there is a star before our opening paren
						else if (i != 0 && exp[i - 1].equals("*")) {
							//first make the subtree with the star
							String top = operator.pop();
							makeSubtree(top);
							printTree(operand.peek());
							//and then push the # operator 
							operator.push("#");
							
							

						}
						
						//finally this will push the (
						operator.push(exp[i]);
					}

					// encounter ) , pop until ) found
					else if (exp[i].equals(")")) {

						//this is the popping until we get the (
						while (!operator.peek().equals("(")) {
							
							//make a subtree out of each popped operator
							String top = operator.pop();
							makeSubtree(top);
						}

						// finally pop off the (
						operator.pop();
					}

					// check precedence for non parenthesis // make sure you
					// check precedence for all the ones on the stack if any are
					// remove

					else if (ops.indexOf((exp[i].charAt(0))) <= ops.indexOf((operator.peek().charAt(0)))) {

						while ((!operator.isEmpty())
								&& ops.indexOf((exp[i].charAt(0))) <= ops.indexOf((operator.peek().charAt(0)))) {
							String top = operator.pop();
							// make a subtree with what is popping off top of
							// stack
							makeSubtree(top);
						}
						operator.push(exp[i]);
					}

					// if greater than, push
					else {
						operator.push(exp[i]);
					}
				}

				// the operator stack is empty 
				else {
					
					//if our expression was a( then we need to concat them together
					if ((i != 0 && alph.contains(exp[i - 1]) && exp[i].equals("("))) {
						operator.push("#");

					}

					//this is when the expression is )( we need to concat them 
					else if (i != 0 && exp[i - 1].equals(")") && exp[i].equals("(")) {

						operator.push("#");

					}

					else if (i != 0 && exp[i - 1].equals("*")) {
						operator.push("#");

					}
					operator.push(exp[i]); // pushing operator if the stack was
					// empty to start

				}

			}

			// handle alphabet
			else {
				Tree sub = new Tree(exp[i]); // creates root

				if ((i != 0) && (alph.contains(exp[i - 1]))) {
					operator.push("#");// implied concatenation because of
					// operand followed by operand

					operand.push(sub);
				}

				//if we are handeling )a we need to concat them 
				else if ((i != 0) && exp[i - 1].equals(")")) {
					
					
					operator.push("#");

					//then push the subtree
					operand.push(sub);
					
				}

				// then we need to make the * and previous letter a tree and
				// push that on the stack
				else if ((i != 0) && (exp[i - 1].equals("*"))) {
					String top = operator.pop();
					// make a subtree with what is popping off top of stack
					makeSubtree(top);
					operator.push("#");

					operand.push(sub);

				} else
					operand.push(sub);
			}
		}
		while (!(operator.isEmpty())) {
			String top = operator.pop();
			// make a subtree with what is popping off top of stack

			makeSubtree(top);
		}

		Tree op = operand.pop();
		
		if (operand.isEmpty()) {
			return op;
		} else {

			
			// printTree(operand.pop());

			writeToFile(1, null);

			System.exit(-1);
			return null;
		}
	}

	/**
	 * This will check if we are dealing with an invalid expression 
	 * 
	 * @param exp our expression 
	 * @param i the index of what 
	 */
	public static void invalidExp(String[] exp, int i) {
		
		//checking to see if we are on the first index and that it is not in either the alphabet or the opperator expressions
		if (i == 0) {
			if (!((exp[i].equals("(")) | (alph.contains(exp[i])))) {
				
				//if there are write invalid expression to the file 
				ArrayList<String> not_needed = new ArrayList<String>();
				writeToFile(1, not_needed);
				System.exit(-1);
			}
		} else {
			
			//check to see if there are two || in a row 
			if (exp[i].equals("|") && exp[i - 1].equals("|")) {

				//if they are then print invalid expression to the file 
				ArrayList<String> not_needed = new ArrayList<String>();
				writeToFile(1, not_needed);
				System.exit(-1);
			}
		}
	}

	/**
	 * Method that will  make a subtree from whatever value is on the top of the operator stack 
	 * 
	 * @param topofStack operator popped off the top of the stack 
	 * 
	 */
	public static void makeSubtree(String topofStack) {
		
		//If the topofStack is a * then we just make a right subtree from this to put on top of the opperand stack 
		if (topofStack.equals("*")) {
			Tree s = operand.pop();
			Tree starTree = new Tree(topofStack, s, null);
			operand.push(starTree);
		} else {
			
			//otherwise we make the left and right subtree from the two top operands on top of the stack 
			Tree right = operand.pop();
			Tree left = operand.pop();
			Tree newTree = new Tree(topofStack, right, left);
			operand.push(newTree);
		}
	}

	/**
	 * Method that will make the NFA expressions based on the input tree
	 * This tree will be the syntax tree 
	 * 
	 * @param t which will be the syntax then the sub trees as needed
	 */
	public static void make_NFA(Tree t) {
		
		//We want to traverse the tree in a depth first manner

		//make the NFA from the left subtree
		if (t.left != null) {
			make_NFA(t.left);
		}

		//make the NFA from the right subtree
		if (t.right != null) {
			make_NFA(t.right);
		}
		

		//Check to see if the expression is the empty string 
		if (t.root.equals("N")) {
			run_NFA();
			return;
		}

		// Check to see if it is a leaf node (which is our base case)
		if (t.isLeafNode()) {

			//The first state is the next state available add this state to our state array this is the start state
			String state1 = (states.size() + 1) + "";
			states.add(state1);
			t.start = state1;
			
			//do the same for the end state 
			String state2 = (states.size() + 1) + "";
			t.end.add(state2);
			states.add(state2);

			//set up our transition 
			String tran = state1 + " '" + t.root + "' " + state2;

			//add the transition to the array 
			transition.add(tran);
			t.transition.add(tran);
			
		}

		//If the root is a hashtag signifying concatination 
		else if (t.root.equals("#")) {
			
			//get the left and right subtrees
			Tree left = t.left;
			Tree right = t.right;

			String tran;
			
				// make trans be left ends to right start as normal
				for (int i = 0; i < t.left.end.size(); i++) {
				tran = left.end.get(i) + " '" + "e" + "' " + right.start;
				transition.add(tran);
				t.transition.add(tran);
				}

				//make the start state the start state of the left
			t.start = left.start;
			
			//make the end states the ends of the last 
			for (int i = 0; i < t.right.end.size(); i++) {
				t.end.add(t.right.end.get(i));
				
			}
		}

		// then we are going to do an epsilon transition back to the start state
		else if (t.root.equals("*")) {
			
			//so we will keep the same start and end states and just add them to the new roots global variables
			Tree child = t.right;
			
			//set start
			t.start = child.start;
			String tran = new String();

			//set end 
			for (int i = 0; i < child.end.size(); i++) {
				t.end.add(child.end.get(i));
				

			}

		
				for (int i = 0; i < t.end.size(); i++) {
					
					//goes from the end of the right child back to the start of the child 
					tran = t.end.get(i) + " '" + "e" + "' " + t.start;
					
					//goes from the start of the child to the end of the child 
					String tran2 = t.start + " 'e' " + t.end.get(i);
					//add to the transitions
					transition.add(tran);
					t.transition.add(tran);
					transition.add(tran2);
					t.transition.add(tran2);
				}
			

		} 
		
		//otherwise we are handeling a union 
		else if (t.root.equals("|")) {
			// we have to create a new start state
			String new_start = (states.size() + 1) + "";
			states.add(new_start);
			
			//set the start state of t 
			t.start = new_start;
			
			//go from the new start to the right start 
			String tran1 = new_start + " '" + "e" + "' " + t.right.start;
			
			//go from the new start to the left start
			String tran2 = new_start + " '" + "e" + "' " + t.left.start;

			//add to transitions
			transition.add(tran1);
			transition.add(tran2);
			t.transition.add(tran1);
			t.transition.add(tran2);

			

			// Now set the new ends by looping through the end array list
			for (int i = 0; i < t.right.end.size(); i++) {
				t.end.add(t.right.end.get(i));
			}
			for (int i = 0; i < t.left.end.size(); i++) {
				t.end.add(t.left.end.get(i));
			}
		}

	}

	/**
	 * The transition are stored in the transition array list The start state is
	 * the roots start The accepts states are the roots ends The states are
	 * stored in the state array The alphabet is stored in alph
	 * 
	 */
	public static void run_NFA() {

		for (int i = 0; i < syntax.end.size(); i++) {
			
		}
		// set Q
		Q = states.size();

		alphabet = alph.split("");

		alph_length = alph.length();
		accept_states = new String[syntax.end.size()]; // initialize array

		// set up the array to store the transitions
		// the index of Q will be 1 less than the state that we are working
		// with (n-1)
		trans = new String[Q + 1][alph_length + 1];

		// while length of input line infers transition
		for (int i = 0; i < transition.size(); i++) {
			setTransitions(transition.get(i));
		}

		// at the line with start state
		start = syntax.start;

		// read in the accept states, store in array
		for (int i = 0; i < syntax.end.size(); i++) {
			accept_states[i] = syntax.end.get(i);
		}

		// Now we need to set the states of our DFA do this by calling the
		// create_list method that will return a map with the transitions
		Map<String, String> dfa_transitions = create_List();

		print_map(dfa_transitions);

	}

	/**
	 * This is a method that will set the transitions of our NFA and add them to
	 * our double array trans
	 * 
	 * @param input
	 *            string which is the string that was read into the file in the
	 *            form qa 'c' qb
	 */
	private static void setTransitions(String input) {

		String[] cur = input.split(" ");

		// the first index will be the current state
		int current_state = Integer.parseInt(cur[0]) - 1;

		// the second index will be scanned letter of the alphabet or e
		String scanned = cur[1];
		char letter = scanned.charAt(1);

		// find where this is in the alphabet

		int alph_index;
		if (letter == 'e') {
			alph_index = alph_length - 1;
		} else {
			alph_index = alph.indexOf(letter);
		}

		// the third index will be the next state
		String next_state = cur[2];

		// now we will update the value at that part of the index adding to the
		// current values if there are any

		String trans_func = trans[current_state][alph_index];
		if (trans_func != null) {
			trans_func = trans_func + "," + next_state;
		} else {
			trans_func = next_state;
		}
		trans[current_state][alph_index] = trans_func;

	}

	/**
	 * Method will set up the key for the map, this key will consist of the
	 * states that we are current in (of our NFA) so this could be 1,2,3 and the
	 * letter of the alphabet that we are looking at ex. a
	 * 
	 * @param state
	 *            which is a string of the states we are in
	 * @param alph
	 *            this index of the the letter of the alphabet that we are in
	 * 
	 * @return String that contains our key, for example it could be 1,2,3a
	 */
	private static String findKey(String state, int alph) {
		String key = state + alphabet[alph];

		return key;
	}

	/**
	 * Method that will create the hashmap that our DFA state transitions are
	 * stored in It will also fill in an array list will all the possible DFA
	 * states where each state consists as a string of NFA states For example
	 * (1,3,5)
	 * 
	 * We will return our hashmap map
	 * 
	 */
	private static Map<String, String> create_List() {
		Map<String, String> map = new HashMap<String, String>();
		String key;

		dfa_states = new ArrayList<String>();

		// we need to use the start state and see where we can get with epsilon
		// transitions to form the start state
		String DFAstart = "" + syntax.start;

		String dfa = trans[Integer.parseInt(syntax.start) - 1][alph_length - 1];

		ArrayList<String> D_start = new ArrayList<String>();

		if (dfa != null) {

			String[] temp1 = dfa.split(",");

			for (int i = 0; i < temp1.length; i++) {
				D_start.add(temp1[i]);
			}

			// Now we are checking if there are any more epsilon transitions out
			// of these states
			for (int k = 0; k < D_start.size(); k++) {

				// now we want to split each string at , and add it to next
				// if it is not already there
				ArrayList<String> n = new ArrayList<String>();

				// checks to see if there are any epsilon transitions ouf of
				// these states
				String ns = trans[Integer.parseInt(D_start.get(k)) - 1][alph_length - 1];

				if (ns != null) {
					// add ns into the array list
					String[] temp = ns.split(",");

					for (int i = 0; i < temp.length; i++) {
						n.add(temp[i]);
					}

					for (int j = 0; j < n.size(); j++) {
						if (!D_start.contains(n.get(j))) {

							D_start.add(n.get(j));

							// Now see if n.get(j) has any epsilon trans
							String np = trans[Integer.parseInt(n.get(j)) - 1][alph_length - 1];

						}
					}
				}
			}
		}

		for (int i = 0; i < D_start.size(); i++) {
			DFAstart = DFAstart + "," + D_start.get(i);
		}
		dfa_states.add(DFAstart);

		// find the remaining states transitions after start has been set
		for (int i = 0; i < dfa_states.size(); i++) {

			// get the next state that we have in our dfa_states
			String s = dfa_states.get(i);

			// Now loop through the alphabet array and add each possible
			// transition
			for (int k = 0; k < alphabet.length - 1; k++) {

				key = findKey(s, k);

				// find were we can go with all the states in the start state
				String map_String = set_Trans_DFA(k, s); // returns next_state

				// string i.e 2,3,4
				if (map_String != null) {

					map.put(key, map_String); // example: 1, [2,3,4]

					if (map_String != null) {
						if (!dfa_states.contains((String) map_String)) {
							dfa_states.add(map_String); // list of all dfa
							// states
						}
					}
				}
			}
		}

		return map;
	}

	/**
	 * This method will set the transitions of a state that we are in for a
	 * specified letter of the alphabet
	 * 
	 * @param int
	 *            a which is the index of the letter of the alphabet we are
	 *            making the transitions for This index corresponds to to our
	 *            alphabet array
	 * 
	 * @param String
	 *            state which is a string of all the possible states that we
	 *            could be in, each one will be checked to see where it could go
	 *            based on that letter of the alphabet
	 * 
	 * @return a string that contains all the possible states our original
	 *         string of states could go to
	 */
	private static String set_Trans_DFA(int a, String state) {

		// for every state in the string of states we need to see where this
		// letter of the alph can take us to;

		// Make sure we are not in the dump state
		if (state.equals("%")) {
			return dump;
		}
		if (state != null) {

			String[] state_arr = state.split(",");
			String next_state;
			ArrayList<String> next = new ArrayList<String>();
			String[] next_arr;
			int state_index;

			// loop through all the states and find the transitions for each one
			// adding them to our next_state string
			for (int i = 0; i < state_arr.length; i++) {

				state_index = Integer.parseInt(state_arr[i]) - 1;
				next_state = trans[state_index][a];

				if (next_state != null) {
					next_arr = next_state.split(",");
					for (int j = 0; j < next_arr.length; j++) {
						next.add(next_arr[j]);
					}
				}
			}

			// make sure there are no duplicates in our next state string and
			// that
			// they are in order
			next.sort(null);

			// we have to check every state in next
			for (int k = 0; k < next.size(); k++) {

				String ns = trans[Integer.parseInt(next.get(k)) - 1][alph_length - 1];
				// To see if there are epsilon transitions
				if (ns != null) {

					// now we want to split each string at , and add it to next
					// if it is not already there
					ArrayList<String> n = new ArrayList<String>();

					// add ns into the array list
					String[] temp = ns.split(",");

					for (int i = 0; i < temp.length; i++) {
						n.add(temp[i]);
					}

					for (int j = 0; j < n.size(); j++) {
						if (!next.contains(n.get(j))) {

							next.add(n.get(j));

							// Now see if n.get(j) has any epsilon trans
							String np = trans[Integer.parseInt(n.get(j)) - 1][alph_length - 1];

							if (np != null) {

								String[] temp2 = np.split(",");
								for (int m = 0; m < temp2.length; m++) {
									if (!n.contains(temp2[m])) {
										n.add(temp2[m]);
									}
								}
							}
						}
					}
				}
			}
			// remove any repeats in next
			for (int j = 0; j < next.size(); j++) {
				for (int k = 0; k < next.size(); k++) {

					// if we are at different indices in the array
					if (k != j && j <next.size()) {
						// if the values are equal at the indicies remove one
						if (next.get(j).equals(next.get(k))) {
							next.remove(k);
						}
					}
				}
			}

			if (!next.isEmpty()) {
				next_state = next.get(0);

				for (int i = 1; i < next.size(); i++) {
					next_state = next_state + "," + next.get(i);
				}
			} else

				// There were no transitions with that letter so we must place
				// in a dump state
				next_state = dump;
			return next_state;
		} else

			return dump;
	}

	/**
	 * Finds all keys and values of transitions Creates list of transitions to
	 * be printed to output Calls writeToFile with transition list
	 * 
	 * @param Map
	 *            map of states alphbet with value next states example of
	 *            key,value : 3a, {2,4,5}
	 * 
	 */
	private static void print_map(Map m) {

		String current_state;
		char current_alph;
		String next_state;
		ArrayList<String> list_to_print = new ArrayList<String>();
		String line;
		// list --> map to find all keys/states

		Set s = m.entrySet();

		Iterator it = s.iterator();

		while (it.hasNext()) {
			Map.Entry e = (Map.Entry) it.next();

			String key = (String) e.getKey(); // key = state
			next_state = (String) e.getValue();// next state

			current_alph = key.charAt(key.length() - 1);

			current_state = key.substring(0, key.length() - 1);

			int qa = dfa_states.indexOf(current_state) + 1;
			int qb = dfa_states.indexOf(next_state) + 1;

			line = qa + " '" + current_alph + "' " + qb; // qa 'c' qb
			list_to_print.add(line); // list of transitions to write to output

		}

		dfa_accept = new ArrayList<String>();

		// sets up the accept array list for the DFA
		for (int i = 0; i < dfa_states.size(); i++) {

			String current = dfa_states.get(i);
			
			// now loop through the accept array and see if current contains any
			// of these states
			for (int j = 0; j < accept_states.length; j++) {

				if (current.contains((accept_states[j]))) {
					// make sure that we have not already added this state to
					// our accept arraylist
					if (!dfa_accept.contains(current)) {

						dfa_accept.add(current);
					}
				}
			}
		}

		String accept = "";

		// now we need to print off the accept array list
		for (int i = 0; i < dfa_accept.size(); i++) {

			int state = dfa_states.indexOf(dfa_accept.get(i)) + 1;

			if (accept.equals("")) {
				accept = state + "";

			} else {
				accept = accept + " " + state;

			}
		}

		// now call the run dfa program with list_to_print
		run_DFA(list_to_print, accept);

	}

	public static void run_DFA(ArrayList<String> list, String accept) {

		B = alphabet.length;

		transitions = new String[dfa_states.size()][B];
		num_transitions = (dfa_states.size()) * B;

		// set acceptingStates array to be the states from dfa_accept

		acceptingStates = new String[dfa_accept.size()];
		for (int i = 0; i < dfa_accept.size(); i++) {
			acceptingStates[i] = dfa_states.indexOf(dfa_accept.get(i)) + 1 + "";

			
		}

		for (int i = 0; i < list.size(); i++) {
			String line = list.get(i);
			String[] temp = new String[3];
			temp = line.split(" ");
			setTransition(temp);
		}

		start_state = 1;

	}

	private static void setTransition(String[] line) {
		int x = Integer.parseInt(line[0]) - 1;
		line[1] = line[1].replace("'", "");
		String tmp = line[1];
		int y = findIndexOf(tmp, alphabet);

		transitions[x][y] = line[2];

		

	}

	private static int findIndexOf(String x, String[] arr) {
		for (int i = 0; i < arr.length; i++) {
			if (x.equals(arr[i]))
				return i;
		}
		return -1;

	}

	private static String check(String line) {
		currentState = 0;
		String[] word = line.split("");

		ArrayList<String> print = new ArrayList<>();

		for (int i = 0; i < word.length; i++) {
			int y = findIndexOf(word[i], alphabet);

			// handle if we are reading in an empty string
			if (y == -1) {
				// loop through our accepting states to see if our start state
				// is in it
				for (int k = 0; k < acceptingStates.length; k++) {
					if ((start_state + "").equals(acceptingStates[k])) {
						return "true";
					} else
						return "false";
				}
			}

			String nextState = transitions[currentState][y];

			// added this because test 19 had null next states
			if (nextState != null) {
				currentState = Integer.parseInt(nextState) - 1;
			}
		}

		/*
		 * for (int i = 0; i < dfa_states.size(); i++) { System.out.println(
		 * "dfa state " + (i + 1) + " nfa state " + dfa_states.get(i)); }
		 */

		Integer finalState = currentState + 1;

		int index = findIndexOf(finalState.toString(), acceptingStates);
		if (index >= 0) {
			return "true";
		} else {
			return "false";
		}
	}

	/**
	 * writes to output file specified in args[1] formatted accordingly to
	 * re1Out.txt
	 * 
	 * @param int
	 *            1 if invalid regular expression is found in input file 0 if
	 *            valid
	 * @param ArrayList<String>
	 *            list of true or false true --> string is in language of
	 *            regular expression false --> string is not in language of
	 *            regular expression
	 */
	public static void writeToFile(int invalid, ArrayList<String> printing) {

		try {
			// This creates a PrintWriter object wrapped around a BufferedWriter
			// object
			File file = new File(output_fileName);
			FileWriter writer = new FileWriter(file);
			BufferedWriter bufferedWriter = new BufferedWriter(writer);
			PrintWriter printWriter = new PrintWriter(bufferedWriter);

			// Invalid expression
			if (invalid == 1) {
				printWriter.println("Invalid expression");
				printWriter.close();
			}

			// in the language
			for (int i = 0; i < printing.size(); i++) {
				
				printWriter.println(printing.get(i));
				System.out.println(printing.get(i));
			}
			printWriter.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}

/**
 * A new tree class that is used to create the syntax tree 
 * 
 * @author julia cassella and maeve m
 * 
 *
 */
class Tree {
	Tree left; //left subtree
	Tree right; //right subtree
	String root; //root of the tree
	ArrayList<String> transition; //transitions for that subtree
	String start; //start states of that tree
	ArrayList<String> end; //end (accept states) of the tree

	/**
	 * Method the creates a tree with just an operand (no right and left children)
	 * @param operand root value
	 */
	public Tree(String operand) {
		root = operand;
		end = new ArrayList<String>();
		transition = new ArrayList<String>();
	}

	/**
	 * Method that sets up a tree with the left and right subtrees specified 
	 * 
	 * @param operand root value 
	 * @param r right subtree
	 * @param l left subtree
	 */
	public Tree(String operand, Tree r, Tree l) {
		root = operand;
		right = r;
		left = l;
		end = new ArrayList<String>();
		transition = new ArrayList<String>();
	}
	
/**
 * Method to set the right child of the tree
 * @param rightChild
 */
	public void setRight(Tree rightChild) {
		right = rightChild;
	}

	/**
	 * Set the left subtree 
	 * @param leftChild
	 */
	public void setLeft(Tree leftChild) {
		left = leftChild;
	}

	/**
	 * Method to check if the tree is a lead node (that means there are no left and right children )
	 * @return
	 */
	public boolean isLeafNode() {
		if ((this.right == null) && (this.left == null))
			return true;
		return false;
	}

}
